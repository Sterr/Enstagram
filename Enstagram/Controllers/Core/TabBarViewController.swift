//
//  TabBarViewController.swift
//  Enstagram
//
//  Created by Евгений Пашко on 29.05.2021.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let email = UserDefaults.standard.string(forKey: "email"),
              let username = UserDefaults.standard.string(forKey: "username") else {
            return
        }
        
        let currentUser = User(username: username, email: email)
        
        // Define VCs
        let homeVC = HomeViewController()
        let exploreVC = ExplorerViewController()
        let cameraVC = CameraViewController()
        let activityVC = NotificationViewController()
        let profileVC = ProfileViewController(user: currentUser)
        
        let navHome = UINavigationController(rootViewController: homeVC)
        let navExplore = UINavigationController(rootViewController: exploreVC)
        let navCamera = UINavigationController(rootViewController: cameraVC)
        let navActivity = UINavigationController(rootViewController: activityVC)
        let navProfile = UINavigationController(rootViewController: profileVC)
        
        navHome.navigationBar.tintColor = .label
        navExplore.navigationBar.tintColor = .label
        navCamera.navigationBar.tintColor = .label
        navActivity.navigationBar.tintColor = .label
        navProfile.navigationBar.tintColor = .label
        
        if #available(iOS 14.0, *) {
            navCamera.navigationItem.backButtonDisplayMode = .minimal
        } else {
            navCamera.navigationItem.backButtonTitle = ""
        }
        
        // Define Tab Items
        navHome.tabBarItem = UITabBarItem(title: "Home", image: UIImage(systemName: "house"), tag: 1)
        navExplore.tabBarItem = UITabBarItem(title: "Explore", image: UIImage(systemName: "magnifyingglass"), tag: 1)
        navCamera.tabBarItem = UITabBarItem(title: "Camera", image: UIImage(systemName: "camera"), tag: 1)
        navActivity.tabBarItem = UITabBarItem(title: "Notifications", image: UIImage(systemName: "bell"), tag: 1)
        navProfile.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(systemName: "person.circle"), tag: 1)
        
        // Set Controllers
        self.setViewControllers([navHome, navExplore, navCamera, navActivity, navProfile], animated: false)
    }
}
