//
//  NotificationViewController.swift
//  Enstagram
//
//  Created by Евгений Пашко on 29.05.2021.
//

import UIKit

class NotificationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Notifications"
        view.backgroundColor = .systemGroupedBackground
    }
}
