//
//  User.swift
//  Enstagram
//
//  Created by Евгений Пашко on 29.05.2021.
//

import Foundation

struct User: Codable {
    let username: String
    let email: String
}
