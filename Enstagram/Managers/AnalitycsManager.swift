//
//  AnalitycsManager.swift
//  Enstagram
//
//  Created by Евгений Пашко on 29.05.2021.
//

import FirebaseAnalytics
import Foundation

final class AnalyticsManager {
    static let shared = AnalyticsManager()
    
    private init() {}
    
    func logEvent() {
        Analytics.logEvent("", parameters: [:])
    }
}
