//
//  PostDatetimeCollectionViewCellViewModel.swift
//  Enstagram
//
//  Created by Евгений Пашко on 03.06.2021.
//

import Foundation

struct PostDatetimeCollectionViewCellViewModel {
    let date: Date
}
