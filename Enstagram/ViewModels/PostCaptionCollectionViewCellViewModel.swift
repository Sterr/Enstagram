//
//  PostCaptionCollectionViewCellViewModel.swift
//  Enstagram
//
//  Created by Евгений Пашко on 03.06.2021.
//

import Foundation

struct PostCaptionCollectionViewCellViewModel {
    let username: String
    let caption: String?
}
