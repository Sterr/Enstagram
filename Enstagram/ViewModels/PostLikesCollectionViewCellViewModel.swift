//
//  PostLikesCollectionViewCellViewModel.swift
//  Enstagram
//
//  Created by Евгений Пашко on 03.06.2021.
//

import Foundation

struct PostLikesCollectionViewCellViewModel {
    let likers: [String]
}
